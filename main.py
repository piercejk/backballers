from flask import Flask, render_template
import subprocess
import simplejson
from flask import jsonify
import os
from models import app
import json
from mlb import msf, get_stat, get_scores, get_standings
from flask import abort
from ohmysportsfeedspy import MySportsFeeds


@app.route("/ballers/api/scores/<date>")
def scores(date):

    daily_scores = msf.msf_get_data(league='mlb',season='current',feed='daily_games',format='json', date = date,force = True)
    cleaned_scores = get_scores(daily_scores['games'])


    return jsonify(cleaned_scores)


@app.route("/ballers/api/standings")
def standings():
    standings = msf.msf_get_data(league = 'mlb', feed = 'seasonal_standings', season = 'current', format = 'json', stats = None)

    standings = get_standings(standings['teams'])

    return jsonify(standings)


@app.route("/ballers/api/team/<team>")
def team(team):
    stats = msf.msf_get_data(league = 'mlb', feed = 'seasonal_team_stats', season = 'current', format = 'json', team = team)

    return jsonify(stats['teamStatsTotals'][0])

@app.route("/ballers/api/stats")
def stats():
    return render_template("stats_static.json")

@app.route("/ballers/api/daily_cron")
def cron():
    #basically this is really going to do what is right above.
    stats = msf.msf_get_data(league = 'mlb', feed = 'seasonal_player_stats',season = 'current', format = 'json')

    allStat = get_stat(stats['playerStatsTotals'])

    os.remove('templates/stats_static.json')

    with open('templates/stats_static.json', 'w') as statfile:
        json.dump(allStat, statfile)

    return 'cron success'



if __name__ == '__main__':
    app.run(debug=True)
