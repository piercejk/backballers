from ohmysportsfeedspy import MySportsFeeds
import simplejson
import json

msf = MySportsFeeds(version="2.0", verbose = True)
msf.authenticate("ea5ffd04-787e-4b3b-a516-0ead12", 'MYSPORTSFEEDS')

leagues = {"AL" : ["SEA", "LAA", "HOU", "TEX", "OAK", "MIN", 'CWS', 'KC', 'CLE', 'DET', 'BOS', 'NYY', 'TB' ,'BAL' , 'TOR'],
            "NL" : ['SF', 'LAD', 'SD', 'ARI', 'COL', 'MIL', 'CIN', 'CHC', 'STL', 'PIT', 'NYM', 'WSH', 'PHI', 'MAR', 'ATL']}

def get_standings(standings):
    # you also need the possibility to do the wild card function.
    #dictionary of arrays: arrays contain the team name, wins, losses, pct g
    std = {"American League" : {}, "National League" : {}, "WC" : {"National League" : [], "American League" : []}}

    #std = {American League : {}}, {National League : {}}, {Wild Card : {}}

    #std['National League WC'] = []
    #std['American League WC'] = []

    for team in standings:

        conference = team['conferenceRank']['conferenceName']
        division = team['divisionRank']['divisionName']

        #ivision = conference + ' ' + division

        if division not in std[conference].keys():
            std[conference][division] = [{}]*5

        rank = int(team['divisionRank']['rank'])

        info = {}
        info['team'] = team['team']['abbreviation']
        info['rank'] = rank
        info['wins'] = team['stats']['standings']['wins']
        info['loses'] = team['stats']['standings']['losses']
        info['winPct'] = round(team['stats']['standings']['winPct'],3)
        info['gamesBack'] = team['divisionRank']['gamesBack']

        std[conference][division][rank-1] = info


        if not rank == 1:
            rank = 0

            if len(std["WC"][conference]) == 0: #ooooh okay you haven't put anything in.
                std["WC"][conference].append(info)
                continue
                #you are not appending anything here I think.

            for spot in std["WC"][conference]:

                if info['winPct'] >= spot['winPct']:
                    break
                rank = rank + 1
            #this references an array
            std["WC"][conference].insert(rank, info)


    wc = ["National League", "American League"]
    for wild in wc:

        std["WC"][wild][0]['wcGamesBack'] = 0.0
        std["WC"][wild][1]['wcGamesBack'] = 0.0


        sp = std["WC"][wild][1]

        for teams in std["WC"][wild][2:]:
            win_df = sp['wins'] - teams['wins']
            loss_df = teams['loses'] - sp['loses']
            tot = win_df + loss_df

            gb = tot/2

            teams['wcGamesBack'] = gb


    return std


def get_scores(data):

    dt = {}

    games = []

    for eachGame in data:

        thisGame = {}
        status = eachGame['schedule']['playedStatus']

        thisGame['status'] = status
        thisGame['homeTeam'] = eachGame['schedule']['homeTeam']['abbreviation']
        thisGame['awayTeam'] = eachGame['schedule']['awayTeam']['abbreviation']


        if status == 'UNPLAYED':
            thisGame['startTime'] = eachGame['schedule']['startTime']

        elif status == 'LIVE':
            thisGame['scores'] = eachGame['score']['innings']
            thisGame['currentInning'] = eachGame['score']['currentInning']
            thisGame['homeScore'] = eachGame['score']['homeScoreTotal']
            thisGame['awayScore'] = eachGame['score']['awayScoreTotal']
            thisGame['homeHits'] = eachGame['score']['homeHitsTotal']
            thisGame['awayHits'] = eachGame['score']['awayHitsTotal']
            thisGame['homeErrors'] = eachGame['score']['homeErrorsTotal']
            thisGame['awayErrors'] = eachGame['score']['awayErrorsTotal']
        else:
            thisGame['scores'] = eachGame['score']['innings']
            thisGame['homeScore'] = eachGame['score']['homeScoreTotal']
            thisGame['awayScore'] = eachGame['score']['awayScoreTotal']
            thisGame['homeHits'] = eachGame['score']['homeHitsTotal']
            thisGame['awayHits'] = eachGame['score']['awayHitsTotal']
            thisGame['homeErrors'] = eachGame['score']['homeErrorsTotal']
            thisGame['awayErrors'] = eachGame['score']['awayErrorsTotal']

        games.append(thisGame)



    dt['data'] = games
    return dt




def get_stat(all):

    top = {"AL" : {}, "NL" : {}, "MLB" : {}}
    stats = {"batting" : ['homeruns', 'battingAvg', 'runsBattedIn'], "pitching" : ['wins', 'earnedRunAvg', 'pitcherStrikeouts']}

    with open('id_map.json') as f:
        play_id = json.load(f)
        f.close()

    for eachPlayer in all:

        first = eachPlayer['player']['firstName']
        last = eachPlayer['player']['lastName']

        if first == "Tom" and last == "Murphy":
            continue


        try:
            id = play_id[first + " " + last]
        except:
            continue

        if eachPlayer['player']['primaryPosition'] == 'P':
            mod = 'pitching'
        else:
            mod = 'batting'

        team = eachPlayer['team']['abbreviation']
        if team in leagues['AL']:
            l_mod = "AL"
        else:
            l_mod = "NL"



        if mod == 'pitching' and float(eachPlayer['stats'][mod]['inningsPitched']) < 30.0:
                continue
        elif mod == 'batting' and float(eachPlayer['stats'][mod]['atBats']) < 75.0:
                continue

        #for a pitcher or hitter this will run through each stat that they have.
        #they shoudl be appended under lists for stats although I'm pretty sure that already happens.
        for stat in stats[mod]: #for each pitching or hitting stat.

            mlb = "MLB"
            if stat not in top[mlb].keys():
                top[mlb][stat] = []


            if len(top[mlb][stat]) < 5:
                top[mlb][stat].append({"firstName" :first, "lastName" : last, "id" : id, stat : eachPlayer['stats'][mod][stat], "team" : team})
                #now it's an array of dictionaries.
            else:

                for place in range(len(top[mlb][stat])): #checks each spot in the array of dictionaries.

                    repIn = float(top[mlb][stat][place][stat])
                    repNew = float(eachPlayer['stats'][mod][stat])

                    if stat == 'earnedRunAvg':
                        h = repNew
                        repNew = repIn
                        repIn = h

                    if repNew >= repIn:
                        first = eachPlayer['player']['firstName']
                        last = eachPlayer['player']['lastName']
                        player = {"firstName" :first, "lastName" : last, "id" : play_id[first + " " + last], stat : eachPlayer['stats'][mod][stat], "team" : team}

                        top[mlb][stat].insert(place, player)
                        del top[mlb][stat][-1]
                        break


            if stat not in top[l_mod].keys():
                top[l_mod][stat] = []


            if len(top[l_mod][stat]) < 5:
                top[l_mod][stat].append({"firstName" :first, "lastName" : last, "id" : id, stat : eachPlayer['stats'][mod][stat], "team" : team})
                #now it's an array of dictionaries.
            else:

                for place in range(len(top[l_mod][stat])): #checks each spot in the array of dictionaries.

                    repIn = float(top[l_mod][stat][place][stat])
                    repNew = float(eachPlayer['stats'][mod][stat])

                    if stat == 'earnedRunAvg':
                        h = repNew
                        repNew = repIn
                        repIn = h

                    if repNew >= repIn:
                        first = eachPlayer['player']['firstName']
                        last = eachPlayer['player']['lastName']
                        player = {"firstName" :first, "lastName" : last, "id" : play_id[first + " " + last], stat : eachPlayer['stats'][mod][stat], "team" : team}

                        top[l_mod][stat].insert(place, player)
                        del top[l_mod][stat][-1]
                        break

    return top
